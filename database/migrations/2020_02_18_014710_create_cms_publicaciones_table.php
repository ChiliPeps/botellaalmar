<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsPublicacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_publicaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('slugurl');
            $table->string('descripcion');
            $table->text('contenido');
            $table->string('epigrafe')->nullable();
            $table->string('dedicatoria')->nullable();

            $table->string('imagen');
            $table->string('thumb');
            $table->string('autor_foto');

            $table->integer('hits')->nullable();

            $table->boolean('publish');

            $table->integer('id_autor')->unsigned()->nullable();
            $table->foreign('id_autor')->references('id')->on('autores');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_publicaciones');
    }
}
