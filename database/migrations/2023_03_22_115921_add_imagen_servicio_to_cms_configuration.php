<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagenServicioToCmsConfiguration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cms_configuration', function (Blueprint $table) {
            $table->string('imagen_servicio')->after('imagen_mantenimiento')->nullable()->default(null); // Imagen servicio

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cms_configuration', function (Blueprint $table) {
            $table->dropColumn('imagen_servicio');
        });
    }
}
