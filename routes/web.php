<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Index
// Route::get('/', 'InicioController@getHost');
// Route::get('/blog', 'InicioController@getHostWithBlog');
Route::get('/',             ['as' => 'inicio',      'uses' => 'InicioController@index'   ]);

Route::get('/editores',  ['as' => 'autores',         'uses' => 'InicioController@autores'   ]);
Route::get('/colaboradores',  ['as' => 'colaboradores',   'uses' => 'InicioController@colaboradores'   ]);
Route::get('/contacto',  ['as' => 'contacto',         'uses' => 'InicioController@contacto'   ]);

//Auth Routes
Auth::routes();

//User Home/Dashboard
// Route::get('/home', 'HomeController@index');

// Route::get('/publicacion/{id_noticia}',  ['as' => 'preview',  'uses' => 'Cms\PreviewController@getNoticiaPreview']);

// Publicaciones
// Route::get('/',      ['as' => 'inicio',     'uses' => 'PublicacionesController@getPublicaciones'  ]);
// Get Noticia Preview (CMS)
Route::get('/preview/{id_noticia}',  ['as' => 'preview',  'uses' => 'Cms\PreviewController@getNoticiaPreview']);

//publicacion individual
Route::get('/publicaciones/{id_publicacion}',  ['as' => 'publicacion',  'uses' => 'PublicacionesController@getPublicacion']);

//autor individual
Route::get('/autores/{id_autor}',  ['as' => 'autor',  'uses' => 'AutoresController@getAutorObras']);

Route::get('/obrasAutor',  ['as' => 'obrasAutor',  'uses' => 'AutoresController@getAutorObras2']);

Route::get('/getAutor',  ['as' => 'getAutor',  'uses' => 'AutoresController@getAutor']);

// contacto correo
Route::post('/contacto_send', ['as' => 'contacto.send',   'uses' => 'ContactoController@enviarCorreo']);

