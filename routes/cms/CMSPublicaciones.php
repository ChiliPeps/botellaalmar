<?php

// Publicaciones Module
Route::get('publicaciones',   ['as' => 'admin.publicaciones.index', 'uses' => 'Cms\PublicacionesController@index']);

// Ajax Routes
Route::group(['middleware' => ['ajax']], function () {

	Route::get('publicaciones/get', ['as' => 'admin.publicaciones.get',      'uses' => 'Cms\PublicacionesController@getPublicaciones']);

	Route::post('publicaciones/set',          ['as' => 'admin.publicaciones.set',      'uses' => 'Cms\PublicacionesController@setPublicacion']);

    Route::post('publicaciones/update',       ['as' => 'admin.publicaciones.update',   'uses' => 'Cms\PublicacionesController@updatePublicacion']);

    Route::delete('publicaciones/delete',     ['as' => 'admin.publicaciones.delete',   'uses' => 'Cms\PublicacionesController@deletePublicacion']);    

    Route::post('publicaciones/publish',       ['as' => 'admin.publicacion.publish',   'uses' => 'Cms\PublicacionesController@publish']);



	});