<?php

//Categorias Module
Route::get('autores',   ['as' => 'admin.autores.index', 'uses' => 'Cms\AutoresController@index']);

//Ajax Routes
Route::group(['middleware' => ['ajax']], function () {
    Route::get('autores/get',           ['as' => 'admin.autores.get',      'uses' => 'Cms\AutoresController@getAutores']);
    Route::post('autores/set',          ['as' => 'admin.autores.set',      'uses' => 'Cms\AutoresController@createAutor']);
    Route::post('autores/update',       ['as' => 'admin.autores.update',   'uses' => 'Cms\AutoresController@updateAutor']);
    Route::delete('autores/delete',     ['as' => 'admin.autores.delete',   'uses' => 'Cms\AutoresController@deleteAutor']);

    Route::get('autores/getAll',           ['as' => 'admin.autores.getAll',      'uses' => 'Cms\AutoresController@getAll']);

    // Para Módulo de Noticias
    Route::get('autores/getAll',        ['as' => 'admin.autores.getAll',      'uses' => 'Cms\AutoresController@getAll']);


	//subir imagenes 
    Route::post('noticias/tinyimages',  ['as' => 'admin.noticias.tinyimages', 'uses' => 'Cms\PublicacionesController@tinymceImages']);
});




