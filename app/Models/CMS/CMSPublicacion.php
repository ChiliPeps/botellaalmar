<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CMSPublicacion extends Model
{
    use SoftDeletes;

    protected $table = 'cms_publicaciones';

    protected $fillable = ['titulo', 'slugurl', 'descripcion', 'contenido', 'imagen', 'thumb', 'epigrafe', 'dedicatoria','id_autor', 'autor_foto', 'publish', 'hits'];

    public function autor()
    {
        return $this->belongsTo('App\Models\CMS\CMSAutor', 'id_autor');
    }
}
