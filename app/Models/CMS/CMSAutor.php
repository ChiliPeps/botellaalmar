<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CMSAutor extends Model
{
    use SoftDeletes;

    protected $table = 'autores';

    protected $fillable = ['nombre', 'apellidos', 'biografia', 'tipo', 'imagen', 'facebook', 'twitter', 'instagram', 'correo', 'url'];

    public function publicaciones()
    {
        return $this->hasMany('App\Models\CMS\CMSPublicaciones', 'id_autor');
    }
}
