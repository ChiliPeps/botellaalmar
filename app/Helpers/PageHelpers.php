<?php

namespace App\Helpers;

use Carbon\Carbon;

class PageHelpers {

    // date("w")
    public static $dias = array(
        0  => "Domingo",
        1  => "Lunes",
        2  => "Martes",
        3  => "Miércoles",
        4  => "Jueves",
        5  => "Viernes",
        6  => "Sábado"
    );

    public static $meses = array(
        1  => "enero",
        2  => "febrero",
        3  => "marzo",
        4  => "abril",
        5  => "mayo",
        6  => "junio",
        7  => "julio",
        8  => "agosto",
        9  => "septiembre",
        10 => "octubre",
        11 => "noviembre",
        12 => "diciembre"
    );

    public static function dateString($date)
    {
        if ($date == null) { return ""; }
        $datestr = strtotime($date);
        $numdia  = date("d", $datestr);
        $month   = date("n", $datestr);
        $year    = date("Y", $datestr);
        $strdate = $numdia." de ".self::$meses[$month]." de ".$year;
        return $strdate;
    }

    public static function dateStringWithDay($date)
    {
        if ($date == null) { return ""; }
        $datestr = strtotime($date);
        $dia     = date("w", $datestr);
        $numdia  = date("d", $datestr);
        $month   = date("n", $datestr);
        $year    = date("Y", $datestr);
        $strdate = self::$dias[$dia]." ".$numdia." de ".self::$meses[$month]." de ".$year;
        return $strdate;
    }

    public static function getTime($date)
    {
        if ($date == null) { return ""; }
        $datestr = strtotime($date);
        $time = date('H:i:s', $datestr);
        return $time;
    }

    public static function dateStringWithDayMongo($createdAt)
    {
        $date = $createdAt->toDateTime()->format(DATE_ATOM);
        $datestr = strtotime($date);
        $dia     = date("w", $datestr);
        $numdia  = date("d", $datestr);
        $month   = date("n", $datestr);
        $year    = date("Y", $datestr);
        $strdate = self::$dias[$dia].", ".$numdia." de ".self::$meses[$month]." de ".$year;
        return $strdate;
    }

    public static function getTimeMongo($createdAt)
    {
        $date = $createdAt->toDateTime()->format(DATE_ATOM);
        $datestr = strtotime($date);
        $dateInLocal = date("h:i", $datestr);
        return $dateInLocal;
    }

    public static function dataUri($data, $mime)
    {
        $base64 = base64_encode($data);
        return ('data:' . $mime . ';base64,' . $base64);
    }

    public static function youtube_url_to_embed($youtube_url)
    {
        $search = '/youtube\.com\/watch\?v=([a-zA-Z0-9]+)/smi';
        $replace = "youtube.com/embed/$1";
        $embed_url = preg_replace($search, $replace, $youtube_url);
        return $embed_url;
    }

    public static function tagsToString($tags)
    {   
        $len = count($tags); $text = "";
        if ($len == 0) { return ""; }
        for ($i = 0; $i < $len; $i++) { 
            if ($i == $len - 1) {
                $text .= $tags[$i]['tag']['nombre'];
            } else {
                $text .= $tags[$i]['tag']['nombre'].", ";
            }
        }
        return $text;
    }
}

?>
