<?php

namespace App\Http\Requests\CMS;

use Illuminate\Foundation\Http\FormRequest;

class CMSPublicacionesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'            => 'required|max:255',
            'descripcion'       => 'required|max:255',
            'contenido'         => 'required',
            // 'autor_foto'        => 'required',
            // 'imagen'            => 'required',
            'id_autor'          => 'required'
        ];
    }
}
