<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'     => 'required|string|max:255',
            'correo'      => 'required|string|email|max:255',
            'comentario' => 'required|string'
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre.required'      => 'El nombre es requerido.',
            'correo.required'      => 'El Correo es requerido.',
            'correo.email'         => 'No es un correo válido.',
            'comentario.required'  => 'El comentario es requerido'
        ];
    }
}
