<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CMS\CMSAutoresRequest;
use App\Models\CMS\CMSAutor as Autor;
use App\Models\CMS\CMSPublicacion as Publicacion;
use Auth;
use Storage;

class AutoresController extends Controller
{
    protected $reportero_error = ['success' => false, 'errorCode' => 'is_reportero'];

    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        return view('cms.autores.index');
    }

    public function getAutores(Request $request)
    {

        if($request->has('tipo') && $request->has('busqueda')) {
            $tipo = $request->tipo;
            $busqueda = $request->busqueda;
            $results = Autor::where($tipo, 'LIKE', '%'.$busqueda.'%')
            ->orderBy('created_at', 'desc')->paginate(20);
        } else {
            $results = Autor::orderBy('created_at', 'desc')->paginate(20);
        }
        return response()->json($results);
    }

    public function getAll()
    {        
        $results = Autor::orderBy('created_at', 'desc')->get();

        return response()->json($results);
    }
    public function createAutor(CMSAutoresRequest $request)
    {
        $autor = Autor::create([
            'nombre'     => $request->nombre,
            'apellidos'  => $request->apellidos,
            'tipo'       => $request->tipo,
            'biografia'  => $request->biografia,
            'imagen'     => "Subiendo...",
            'facebook'   => $this->checkNull($request->facebook),
            'twitter'    => $this->checkNull($request->twitter),
            'instagram'  => $this->checkNull($request->instagram),
            'correo'     => $this->checkNull($request->correo),
            'url'        => $this->checkNull($request->url)
        ]);

        // Upload File
        if($request->hasFile('imagen')) {
            $path = $this->uploadImage($request->file('imagen'), $autor->id);
            $autor->imagen = $path;
            $autor->save();
        }

        return response()->json($autor);
    }

    public function updateAutor(CMSAutoresRequest $request)
    {   
        $autor = Autor::findOrFail($request->id);

        // Update
        $autor->fill([
            'nombre'     => $request->nombre,
            'apellidos'  => $request->apellidos,
            'tipo'       => $request->tipo,
            'biografia'  => $request->biografia,
            'facebook'   => $this->checkNull($request->facebook),
            'twitter'    => $this->checkNull($request->twitter),
            'instagram'  => $this->checkNull($request->instagram),
            'correo'     => $this->checkNull($request->correo),
            'url'        => $this->checkNull($request->url)
        ]);
        $autor->save();

         // Upload New Image
         if($request->hasFile('imagen')) {
            Storage::delete($autor->imagen);
            $path = $this->uploadImage($request->file('imagen'), $autor->id);
            $autor->imagen = $path;
            $autor->save();
        }

        return response()->json($autor);
    }

    public function deleteAutor(Request $request)
    {
        if ($this->isReportero()) { return response()->json($this->reportero_error, 422); }

        // delete all publicaciones of Autor
        $publicaciones = Publicacion::where('id_autor', $request->id)->get();
        $publicaciones->each->delete();
         // DELETE AUTOR
        $autor = Autor::findOrFail($request->id);
        $autor->delete();

        

        return response()->json(true);
    }

    protected function checkNull($value) {
        if ($value == "null") { return ""; }
        else { return $value; }
    }

    public function getAllAutores() {
        $results = Autor::orderBy('created_at', 'desc')->get();
        return response()->json($results);
    }

    protected function uploadImage($file, $autor_id)
    {       
        // Directories
        $directory_name = "media-manager";
        $directory      = $directory_name."/autores/".$autor_id;

        // Save Imagen
        $file_name = $autor_id."_".time().".".$file->getClientOriginalExtension();
        $path = $file->storeAs($directory, $file_name, 'public');
        return $path;
    }

    protected function isReportero() {
        if (Auth::guard('cms')->user()->type == 'reportero') { return true; }
        else { return false; }
    }
}
