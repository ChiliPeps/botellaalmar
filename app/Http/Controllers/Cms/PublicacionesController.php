<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSPublicacion;
use App\Models\CMS\CMSAutor;

use App\Http\Requests\CMS\CMSPublicacionesRequest;

use Image;
use Storage;

class PublicacionesController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function index()
    {
        return view('cms.publicaciones.index');
    }

    public function getPublicaciones(Request $request)
    {   
        
        if($request->has('tipo') && $request->has('busqueda')) {
            $tipo = $request->tipo;
            $busqueda = $request->busqueda;

            if ($tipo == 'nombre' || $tipo == 'apellidos') {

                $results = CMSPublicacion::with('autor')->whereHas('autor', function ($q) use ($tipo, $busqueda){
                    $q->where($tipo,'LIKE', '%'.$busqueda.'%');
                })->orderBy('created_at', 'desc')->paginate(20);
            }
            else{
                // dd($request->all());
                $results = CMSPublicacion::with('autor')->where($tipo, 'LIKE', '%'.$busqueda.'%')->orderBy('created_at', 'desc')->paginate(20);
                // dd($results);
            }
        } else {
            $results = CMSPublicacion::with('autor')->orderBy('created_at', 'desc')->paginate(20);
        }
        return response()->json($results);

        // $withs = ['categoria', 'tags.tag', 'user', 'programada'];
        // if($request->has('tipo') && $request->has('busqueda')) {

        //     $tipo = $request->input('tipo');
        //     $busqueda = $request->input('busqueda');

        //     if ($tipo == 'categoria') {
        //         $categoria = Categoria::where('nombre', 'LIKE', '%'.$busqueda.'%')->select('id', 'nombre')->get();
        //         $cat_ids = array();
        //         foreach ($categoria as $value) { $cat_ids[] = $value->id; }
        //         $results = Noticia::with($withs)->whereIn('id_categoria', $cat_ids)
        //         ->orderBy('created_at', 'desc')->paginate(20);
        //     } else {
        //         if ($tipo == 'fecha') {
        //             $results = Noticia::with($withs)->where('fecha', $busqueda)
        //             ->orderBy('created_at', 'desc')->paginate(20);
        //         } else {
        //             $results = Noticia::with($withs)->where($tipo, 'LIKE', '%'.$busqueda.'%')
        //             ->orderBy('created_at', 'desc')->paginate(20);
        //         }
        //     }
        // } else {
            // $results = CMSPublicacion::with('autor')->orderBy('created_at', 'desc')->paginate(20);
        // }

            // dd($results);
        return response()->json($results);
    }

    public function setPublicacion(CMSPublicacionesRequest $request)
    {
    	$publicacion = CMSPublicacion::create([
    		'titulo' 		=> $request->titulo,
    		'slugurl'       => $request->slugurl,
    		'descripcion' 	=> $request->descripcion,
    		'contenido' 	=> $request->contenido,
    		'epigrafe' 		=> $request->epigrafe,
    		'dedicatoria' 	=> $request->dedicatoria,
    		'id_autor'		=> $this->checkNull($request->id_autor),
    		// 'imagen' 		=> 'subiendo...',
    		// 'thumb' 		=> 'subiendo...',
    		'autor_foto'	=> $request->autor_foto,
    		'hits'			=> 0,
    		'publish' 		=> $request->publish,
    	]);

    	// Upload File
        if($request->hasFile('imagen')) {
            $paths = $this->uploadImage($request->file('imagen'), $publicacion->id);
            $publicacion->imagen = $paths['imagen'];
            $publicacion->thumb  = $paths['thumb'];
            $publicacion->save();
        }

        return response()->json(['success' => true, 'publicacion_id' => $publicacion->id]);

    }

    public function updatePublicacion(CMSPublicacionesRequest $request)
    {
    	$publicacion = CMSPublicacion::findOrFail($request->id);
        // Update
        $publicacion->fill([
            'titulo' 		=> $request->titulo,
    		'slugurl'       => $request->slugurl,
    		'descripcion' 	=> $request->descripcion,
    		'contenido' 	=> $request->contenido,            
    		'epigrafe'      => $this->checkNull($request->epigrafe),
    		'dedicatoria' 	=> $this->checkNull($request->dedicatoria),
    		'id_autor'		=> $request->id_autor,
    		'autor_foto'	=> $this->checkNull($request->autor_foto),
    		'publish' 		=> $request->publish,
        ]);
        $publicacion->save();

        // Upload New Image
        if($request->hasFile('imagen')) {
            Storage::delete($publicacion->imagen);
            Storage::delete($publicacion->thumb);
            $paths = $this->uploadImage($request->file('imagen'), $publicacion->id);
            $publicacion->imagen = $paths['imagen'];
            $publicacion->thumb  = $paths['thumb'];
            $publicacion->save();
        }

        // Update Tags
        // $this->tagManager("update", $publicacion->id);

        return response()->json(['success' => true, 'publicacion_id' => $publicacion->id]);
    }

    public function publish(Request $request)
    {
    	$noticia = CMSPublicacion::findOrFail($request->id);
        if ($noticia->publish == 0) { $noticia->publish = 1; } 
        else { $noticia->publish = 0; }
        $noticia->save();
        return response()->json(['success' => true, 'publish' => $noticia->publish]);
    }

    public function deletePublicacion(Request $request)
    {
    	$noticia = CMSPublicacion::findOrFail($request->id);
        //Storage::delete($archivo->ruta); // Softdelete Activated
        //Storage::delete($archivo->ruta); // Softdelete Activated
        $id_noticia = $noticia->id; // Guardar Id
        $noticia->delete();
        return response()->json(['success' => true, 'id_noticia' => $id_noticia]);
    }

    protected function uploadImage($file, $publicacion_id)
    {       
        // Directories
        $directory_name = "media-manager";
        $directory      = $directory_name."/publicaciones/".$publicacion_id;

        // Save Imagen
        $file_name = $publicacion_id."_".time().".".$file->getClientOriginalExtension();
        $path = $file->storeAs($directory, $file_name, 'public');

        // Thumb ---->
        $img = Image::make($file->getRealPath());
        // $img->resize(186, 125); // Antigua Resolución
        $img->resize(200, 120);
        $file_name_thumb = $publicacion_id."_".time()."_thumb.".$file->getClientOriginalExtension();
        $path_thumb = $directory."/".$file_name_thumb;
        $img->save($path_thumb);

        return ['imagen' => $path, 'thumb' => $path_thumb];
    }

    public function tinymceImages(Request $request)
    {
        // Directory
        $directory_name = "media-manager";
        $directory      = $directory_name."/tinymce_images/";
        $file           = $request->file('file');

        // Save Imagen
        $file_name = uniqid('img_').".".$file->getClientOriginalExtension();
        $path = $file->storeAs($directory, $file_name, 'public');
        return response()->json(url($path));
    }

    protected function checkNull($value) {
        if ($value == "null") { return ""; }
        else { return $value; }
    }
}
