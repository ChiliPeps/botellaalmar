<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMS\CMSPublicacion;
use App\Models\CMS\CMSAutor;


class PreviewController extends Controller
{
    public function __construct() 
    {
        $this->middleware('CMSAuthenticate');
    }

    public function getNoticiaPreview($id_publicacion)
    {
        // Check Categoria
        // $w = ['id' => $id_categoria];
        // $categoria = Categoria::where($w)->select('id', 'nombre', 'slug')->first();
        // if($categoria == null) { abort(404); }

        // Get Noticia - Tags - Categoria
        $w = ['id' => $id_publicacion];
        $publicacion = CMSPublicacion::where($w)->first();
        if($publicacion == null) { abort(404); }

        $mismoAutor = CMSPublicacion::with('autor')->where([
            ['id_autor', $publicacion->id_autor],
            ['id', '<>', $publicacion->id]
        ])->take(3)->get();

        return view('pages.preview', compact('publicacion','mismoAutor'));
    }
}
