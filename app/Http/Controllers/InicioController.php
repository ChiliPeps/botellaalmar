<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CMS\CMSPublicacion;
use App\Models\CMS\CMSAutor;
use Carbon\Carbon;

class InicioController extends Controller
{
    public function getHost(Request $request) {
        dd($request->url());
    }

    public function getHostWithBlog(Request $request) {
        $full_url = $request->fullUrl();
        $domain = parse_url($full_url);
        dd($domain['host']);
    }

    public function index()
    {   
        $date = Carbon::now();
        $lastMonth = Carbon::now()->subMonths(2);

        $w = ['publish' => 1];
        $publicaciones = CMSPublicacion::with('autor')->where($w)->skip(1)->take(5)->orderBy('created_at', 'desc')->get();
        $principal = CMSPublicacion::with('autor')->latest('created_at')->where($w)->first();
        $masLeidos = CMSPublicacion::with('autor')->where($w)->orderBy('hits', 'desc')->take(3)->get();

        
        $leidasMes = CMSPublicacion::with('autor')->whereBetween('created_at', [$lastMonth, $date])
        ->orderBy('hits', 'desc')->take(3)->get();

        // dd($noticias ,$date, $lastLastMonth);


        return view('pages.inicio', compact('publicaciones', 'principal', 'masLeidos','leidasMes'));
    }

    public function autores()
    {
        $autores = CMSAutor::where('tipo', 'editor')->get();
        return view('pages.autores', compact('autores'));
    }

    public function colaboradores()
    {
        $colaboradores = CMSAutor::where('tipo', 'colaborador')->get();
        return view('pages.colaboradores', compact('colaboradores'));
    }

    public function contacto()
    {
        return view('pages.contacto');
    }

}
