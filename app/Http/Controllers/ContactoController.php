<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactoRequest;
use App\Models\CMS\CMSConfiguration;
use App\Mail\ContactoCorreo;
use Mail;

class ContactoController extends Controller
{
    public function enviarCorreo(ContactoRequest $request)
    {
    	$conf = CMSConfiguration::first();
        if ($conf == null) { return response()->json(['nodata' => true, 'error' => 'No hay configuración.'], 422); }
        Mail::to($conf->correo_contacto)->send(new ContactoCorreo($request));
        return response()->json(['success' => true]);
    }
}
