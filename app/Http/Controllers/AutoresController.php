<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CMS\CMSPublicacion;
use App\Models\CMS\CMSAutor;

class AutoresController extends Controller
{
	public function getAutorObras($id_autor)
    {
        $w = ['id_autor' => $id_autor];
        $with = ['autor'];
        $publicaciones = CMSPublicacion::with($with)->where($w)->paginate(9);
        if($publicaciones == null) { abort(404); }

        return view('pages.autor', compact('publicaciones'));
    }

    public function getAutorObras2(Request $request)
    {
    	$w = ['id_autor' => $request->id_autor];
        $with = ['autor'];

        if($request->has('tipo') && $request->has('busqueda')) {
            $tipo = $request->tipo;
            $busqueda = $request->busqueda;
            $results = CMSPublicacion::with($with)->where([
            	[$tipo, 'LIKE', '%'.$busqueda.'%'],
            	[$w],
            ])
            ->orderBy('created_at', 'desc')->paginate(3);
        } else {
            $results = CMSPublicacion::with($with)->where($w)->paginate(3);
        }

        return response()->json($results);
    }

    public function getAutor(Request $request)
    {
        $id_autor = $request->id_autor;

        $autor = CMSAutor::where('id', $id_autor)->get();
        return response()->json($autor);

    }
}
