<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CMS\CMSPublicacion;

class PublicacionesController extends Controller
{
    public function getPublicacion($id_publicacion)
    {

        // Incrementar Hits
        CMSPublicacion::where('id', $id_publicacion)->increment('hits');

       // get Publicacion principal
        $w = ['id' => $id_publicacion, 'publish' => 1];
        $with = ['autor'];
        $publicacion = CMSPublicacion::with($with)->where($w)->first();
        if($publicacion == null) { abort(404); }

        // get otras publicaciones del autor
        $mismoAutor = CMSPublicacion::with($with)->where([
            ['id_autor', $publicacion->id_autor],
            ['id', '<>', $publicacion->id]
        ])->take(3)->orderBy('created_at', 'desc')->get();

        $line = $publicacion->titulo;
        // $texto = trim(strip_tags($line));
        $texto = $this->rip_tags($line);
        $linetxt =  mb_convert_encoding($texto, "UTF-8");

        return view('pages.publicacion', compact('publicacion', 'mismoAutor', 'linetxt'));

    }

    public function rip_tags($string)
    {
        // ----- remove HTML TAGs -----
    $string = preg_replace ('/<[^>]*>/', ' ', $string);
   
    // ----- remove control characters -----
    $string = str_replace("\r", '', $string);    // --- replace with empty space
    $string = str_replace("\n", ' ', $string);   // --- replace with space
    $string = str_replace("\t", ' ', $string);   // --- replace with space
   
    // ----- remove multiple spaces -----
    $string = trim(preg_replace('/ {2,}/', ' ', $string));
   
    return $string;
    }
}
