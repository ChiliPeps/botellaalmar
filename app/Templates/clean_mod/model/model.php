<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class CMSC3XlcsX extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cms_C2XpX';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ ];
}
