<div class="fixed-top color1 navbar-dark" id="navbar">
  <nav class="navbar navbar-expand-lg container">
    <a class="navbar-brand" href="index.php"><img src="Imagenes/botella.png" width="55" height="55" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link text" href="autores.php"><i class="fas fa-user-circle"></i> Autores</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link text" href="contacto.php"><i class="fas fa-envelope"></i> Contacto</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#"><i class="fab fa-whatsapp fa-lg"></i> whatsapp</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="#"><i class="fab fa-facebook fa-lg"></i> facebook</a>
        </li>
      </ul>
    </div>
  </nav>
</div>