<!doctype html>
<html lang="es">

<head>

	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Botella al mar es un medio electónico de cultura y politica con sede en La paz, BCS. MÉXICO">
	<meta name="keywords" content="botellaalmar, poesia, política, la paz, BCS, Audiovisuales Dunas,">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-7C2G0M6BMQ"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-7C2G0M6BMQ');
	</script>
	</script>


	{{-- Titulo --}}
	<title> @yield('title') Botella al mar ©. La paz B.C.S. México</title>

	<!-- Metas -->
	@hasSection('metas')
		@yield('metas')
	@else
        <meta property="fb:app_id" content=""/>
		<meta property="og:title" content="Botella al mar. La Paz, Baja California Sur">
		<meta property="og:type" content="website">
		<meta property="og:url" content="https://botellaalmar.com.mx"/>
		<meta property="og:description" content="www.botellaalmar.com.mx es una producción de Servicios Editoriales y Producciones Audiovisuales Dunas, S. de R. L. de C. V.">
		<meta property="og:image" content="{{ url('/')."/images/botella.png" }}"/>
	@endif

	{!! Html::favicon('favicon.ico') !!}
	@include('inc/header_common')

</head>
<body class="body">
	@include('inc/header')

	<!-- Content -->
	@yield('content')
	<!-- Content -->

	@include('inc/footer')
	@include('inc/footer_common')
</body>
</html>