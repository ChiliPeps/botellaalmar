{{-- comentarios de facebooks --}}
{{-- <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.3&appId=955670191154464&autoLogAppEvents=1"></script> --}}


<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v10.0" nonce="dlZPHAgZ"></script>

{{-- Header --}}
<div class="fixed-top color1 navbar-dark" id="navbar">
    <nav class="navbar navbar-expand-lg container">
      <a class="navbar-brand" href="{{route('inicio')}}"><img src="{{url('/images/botella.png')}}" width="55" height="55" alt=""></a>

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link text" href="{{route('autores')}}"><i class="fas fa-user-circle"></i> Editor</a>
          </li>
          {{-- <li class="nav-item active">
            <a class="nav-link text" href="{{route('colaboradores')}}"><i class="fas fa-user-circle"></i> Colaboradores</a>
          </li> --}}
          <li class="nav-item active">
            <a class="nav-link text" href="{{route('contacto')}}" ><i class="fas fa-envelope"></i> Contacto</a>
          </li>
          {{-- <li class="nav-item active">
            <a class="nav-link" href="https://api.whatsapp.com/send?phone=526121527682&text=hola,%20qué%20tal?" target="_blank"><i class="fab fa-whatsapp fa-lg"></i> Whatsapp</a>
          </li> --}}
          <li class="nav-item active">
            <a class="nav-link" href="https://www.facebook.com/botellalmar/" target="_blank"><i class="fab fa-facebook fa-lg"></i> Facebook</a>
          </li>
        </ul>
      </div>
    </nav>
</div>

{{-- <header>
  <div class="color2 header_superior">
    <img class="img_header" src="{{url('/images/header_logo.svg')}}" alt="images/logo.svg">


  </div>
  <div class="header_inferior color3">
    <h1 class="Atlane">medio electronico de cultura y politica</h1>
  </div>
</header> --}}