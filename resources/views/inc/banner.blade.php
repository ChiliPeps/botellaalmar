<header>
	<div class="color2 header_superior">
		<img class="img_header" src="{{url('/images/header_logo.svg')}}" alt="images/logo.svg">
	</div>

	<div class="header_inferior color3">
		<h1 class="Atlane">Textos y Pretextos</h1>
	</div>
</header>