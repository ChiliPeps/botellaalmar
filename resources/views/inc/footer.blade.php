	<footer class="text-white color5">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-lg-8 row pt-4 mt-4">
						<div class="col-lg-5 d-flex justify-content-center">
							<img class="img-fluid" src="{{url('/images/Botella_footer.svg')}}" alt="images/header_logo.svg">

							<ul class="list-group">
								<li class="footerLista text3">La Paz, Baja California Sur, México</li>
								{{-- <li class="footerLista text3">612 114 71 46</li> --}}
								<li class="footerLista text4">manuellucero760@gmail.com</li>
							</ul>
						</div>
						<div class="col-lg-5  col-sm-5 d-flex">
							<ul class="list-group text-md-center text-sm-center">
								{{-- <li class="footerLista text6">Servicios de programación: Komvac</li>
								<li class="footerLista text6">Ilustraciones: El Nane</li>
								<li class="footerLista text6">Diseño: Dafne Torres</li> --}}
							</ul>
						</div>
						<div class="col-lg-2  col-sm-2 d-flex justify-content-center">
							<ul class="list-group"  style="display: inline;">
								<li class="footerLista" style="display: inline;"><a class="text-white" href="#"><i class="fab fa-whatsapp fa-lg"></i></a></li>
								<li class="footerLista" style="display: inline;"><a class="text-white"href="https://www.facebook.com/botellalmar/" target="_blank"><i class="fab fa-facebook fa-lg"></i> </a></li>
								<li class="footerLista" style="display: inline;"><a class="text-white" target="_blank"><i class="fab fa-twitter fa-lg"></i> </a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="small text-center py-3 text8">
				{{-- www.botellalmar.com es una producción de Servicios Editoriales y Producciones Audiovisuales Dunas, S. de R.L. de C.V. Gerente administrador: José Manuel Lucero Higuera.Todos los registros en trámite. --}}
				Responsable de la publicación: Manuel Lucero.
			</div>
	</footer>
</body>
</html>