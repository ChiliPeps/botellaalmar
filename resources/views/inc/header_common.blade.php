<!-- CSS Resources -->
{!! Html::style('css/bootstrap/bootstrap.min.css') !!}
{!! Html::style('css/font-awesome/all.min.css') !!}
{!! Html::style('css/style.css') !!}
{!! Html::style('css/bootstrap-social.css') !!}

 <link rel = "stylesheet"
   type = "text/css"
   href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />


{{-- Stack for CSS --}}
@stack('css')