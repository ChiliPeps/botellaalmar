<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

{!! Html::script('js/bootstrap/bootstrap.min.js') !!}
{!! Html::script('js/font-awesome/all.min.js') !!}

<!-- Vue.js & Vue-Resource -->
{{-- {!! Html::script('plugins/vue/vue.js') !!} --}}
{!! Html::script('plugins/vue/vue.min.js') !!}
{!! Html::script('plugins/vue-resource/vue-resource.min.js') !!}


<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Botella al mar",
    "url": "https://www.botellaalmar.com.mx",
    "address": "",
    "sameAs": [
      "https://www.facebook.com/botellalmar/"
    ]
  }
</script>

{{-- Stack Scripts --}}
@stack('scripts')