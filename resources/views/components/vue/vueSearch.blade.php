{{-- Requerimientos: Bootstrap 3, Vue.js 2, VueHelperFunctions.blade.php -> Lodash.js, Laravel 5.3 --}}
{{-- Componente editado de VueFilterSearch para el proyecto de botella al mar --}}

<script>
Vue.component('filtersearch', {
    props: ['selected', 'options'],
    template: `
    <div class="row">
        <div class="col-md-12 pt-5">

            <div class="input-group">
                <label for="busqueda"></label>
                <input label="Busqueda" type="text" class="form-control size" autocomplete="off" v-model="busqueda" placeholder="{{trans('cms.search_bar')}}">
                <span class="input-group-btn">
                    <button class="btn colorbutton sizeButton" type="button">
                        <img src="../images/catalejo.png" class="img-fluid">
                    </button>
                </span>
            </div>

        </div>
        <div class="col-md-4 invisible">

            <label for="tipo">Por</label>                        
            <select id="tipo" name="tipo" class="form-control" v-model="tipo" @change="filterSearch_tipo">
                <option v-for="o in options" :value="o.value">@{{ o.text }}</option>
            </select>

        </div>
    </div>
    `,
    mounted: function () {
        this.tipo = this.selected;
    },
    data: function () {
        return {
            tipo: 'titulo',
            busqueda: ''           
        }
    },
    watch: {
        busqueda: function () {
            this.filterSearch();
        }
    },
    methods: {
        //Filter Search
        filterSearch: _.debounce(function () {
            this.$emit("updatefilters", { busqueda: this.busqueda, tipo: this.tipo });
        }, 500),

        filterSearch_tipo: _.debounce(function () {
            if(this.busqueda == '') { return; }
            this.$emit("updatefilters", { busqueda: this.busqueda, tipo: this.tipo });
        }, 500)
    }
});
</script>

{{-- CSS --}}
@push('css')
<style>
    .size{
        height: 70px;
    }

    .sizeButton{
        height: 70px;
        width: 70px;
    }

    .colorbutton{
        background-color: #074a8a
    }
</style>
@endpush