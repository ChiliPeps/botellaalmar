{{-- TinyMCE --}}
{!! Html::script('plugins/tinymce/tinymce.min.js') !!}

<script>
Vue.component('tinymce', {
    props: ['value', 'imageuploadurl'],
    template: `<textarea></textarea>`,
    mounted: function () {
        var vm = this;

        // Youtube Plugin
        tinymce.PluginManager.add('youtube_url', function (editor, url) {
            var icon_url="{{url('img/YouTube-icon.png')}}";

            editor.on('init', function (args) {
                editor_id = args.target.id;
            });

            editor.addButton('youtube_url', {
                text:false,
                icon: true,
                image:icon_url,
                onclick: function () {
                    editor.windowManager.open({
                        title: 'Youtube Helper',
                        body: [
                            {   
                                type: 'textbox', name: 'youtube', label: 'Youtube URL'
                            },
                            {
                                type: 'listbox', name: 'align', label: 'Alineación', values: [
                                    { text: 'Izquierda', value: 'left' },
                                    { text: 'Centro',    value: 'center' }
                                ]
                            }
                        ],
                        onsubmit: function(e) {
                            var videoId = '';
                            var url = e.data.youtube;
                            var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
                            var match = url.match(regExp);

                            if (match && match[2].length == 11) {
                                videoId = match[2];
                                var iframeMarkup = '<iframe width="560" height="315" src="//www.youtube.com/embed/' 
                                + videoId + '" frameborder="0" allowfullscreen></iframe>';
                                
                                if (e.data.align == 'left') {
                                    tinyMCE.activeEditor.insertContent('<div class="div_border yutub">' + iframeMarkup + '</div>');
                                } else {
                                    tinyMCE.activeEditor.insertContent('<div class="div_border yutub"><center>' + iframeMarkup + '</center></div>');
                                }
                            } else {
                                alert('error');
                            }
                        }
                    });
                }
            });
        });
        
        // Twitter Plugin
        tinymce.PluginManager.add('twitter_url', function(editor, url) {
            var icon_url="{{url('img/twitter-50.png')}}";

            editor.on('init', function (args) {
                editor_id = args.target.id;
            });

            editor.addButton('twitter_url', {
                text:false,
                icon: true,
                image:icon_url,
                onclick: function () {
                    editor.windowManager.open({
                        title: 'Twitter oEmbed',
                        body: [
                            {   
                                type: 'textbox', name: 'twitter', label: 'Twitter URL'
                            },
                            {
                                type: 'textbox', name: 'ancho', label: 'Ancho', value: '300'
                            },
                            {
                                type: 'label', name: 'labelindo', text: 'Este valor es opcional, debe ser mayor que 180 y menor a 1200.'
                            },
                            {
                                type: 'listbox', name: 'align', label: 'Alineación', values: [
                                    { text: 'Izquierda', value: 'left' },
                                    { text: 'Centro',    value: 'center' },
                                    { text: 'Derecha',   value: 'right' }
                                ]
                            }
                        ],
                        onsubmit: function(e) {
                            var tweetEmbedCode = e.data.twitter;
                            var tweetWidth = e.data.ancho;
                            var tweetAlign = e.data.align;
                            $.ajax({
                                url: "https://publish.twitter.com/oembed?url="+tweetEmbedCode+"&omit_script=true&maxwidth="+tweetWidth+"&align="+tweetAlign,
                                dataType: "jsonp",
                                async: false,
                                success: function(data) {
                                    tinyMCE.activeEditor.insertContent('<div class="div_border" style="marign-bottom: 30px;" contenteditable="false">' + data.html + '</div>');
                                },
                                error: function (jqXHR, exception) {
                                    errorsMsg(jqXHR, exception);
                                },
                            });
                        }
                    });
                }
            });
        });

        // Facebook Plugin
        tinymce.PluginManager.add('facebook_url', function(editor, url) {
            var icon_url="{{url('img/46-facebook-512.png')}}";

            editor.on('init', function (args) {
                editor_id = args.target.id;
            });

            editor.addButton('facebook_url', {
                text:false,
                icon: true,
                image:icon_url,
                onclick: function () {
                    editor.windowManager.open({
                        title: 'Facebook oEmbed',
                        body: [
                            {   
                                type: 'textbox', size: 40, height: '100px',
                                name: 'facebook', label: 'Facebook URL'
                            },
                            {
                                type: 'textbox', size: 40, height: '100px',
                                name: 'ancho', label: 'Ancho', value: '400'
                            },
                            {
                                type: 'label', name: 'labelindo', text: 'Ancho máximo de los medios devueltos. Este valor es opcional y debe ser mayor que 320.'
                            }
                        ],
                        onsubmit: function(e) {
                            var faceEmbedCode = e.data.facebook;
                            var faceWidth = e.data.ancho;
                            $.ajax({
                                url: "https://www.facebook.com/plugins/post/oembed.json/?url="+faceEmbedCode+"&omitscript=true&maxwidth="+faceWidth,
                                dataType: "jsonp",
                                async: false,
                                success: function(data){
                                    tinyMCE.activeEditor.insertContent('<div class="div_border" style="marign-bottom: 30px;" contenteditable="false">' + data.html + '</div>');
                                },
                                error: function (jqXHR, exception) {
                                    errorsMsg(jqXHR, exception);
                                },
                            });
                        }
                    });
                }
            });
        });

        // Instagram Plugin
        tinymce.PluginManager.add('instagram_url', function(editor, url) {
            var icon_url="{{url('img/instagram-icon.png')}}";

            editor.on('init', function (args) {
                editor_id = args.target.id;
            });

            editor.addButton('instagram_url', {
                text:false,
                icon: true,
                image:icon_url,
                onclick: function () {
                    editor.windowManager.open({
                        title: 'Instagram oEmbed',
                        body: [
                            {   
                                type: 'textbox', size: 40, height: '100px',
                                name: 'instagram', label: 'Instagram URL'
                            },
                            {
                                type: 'textbox', size: 40, height: '100px',
                                name: 'ancho', label: 'Ancho', value: '400'
                            },
                            {
                                type: 'label', name: 'labelindo', text: 'Ancho máximo de los medios devueltos. Este valor es opcional y debe ser mayor que 320.'
                            }
                        ],
                        onsubmit: function(e) {

                            var instagramEmbedCode = e.data.instagram;
                            var instagramWidth = e.data.ancho;

                            $.ajax({
                                url: "https://api.instagram.com/oembed?url="+instagramEmbedCode+"&omitscript=true&maxwidth="+instagramWidth,
                                dataType: "json",
                                async: false,
                                success: function(data){
                                    tinyMCE.activeEditor.insertContent('<div class="div_border" style="marign-bottom: 30px;" contenteditable="false">' + data.html + '</div>');
                                },
                                error: function (jqXHR, exception) {
                                    errorsMsg(jqXHR, exception);
                                }
                            });
                        }
                    });
                }
            });
        });
        
        // Errores de ajax de los plugins
        function errorsMsg(jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else { msg = 'Uncaught Error.\n' + jqXHR.responseText; }
            alert(msg);
        }

        // Init tinymce
        tinymce.init({
            selector: '#'+vm.$el.id,
            // copiar en texto plano
            plugins : "paste",
            paste_as_text: true,
            // copiar en texto plano
            //menubar: false,
            language: 'es_MX',
            height: 500,
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc',
                'twitter_url facebook_url instagram_url youtube_url'
            ],
            image_caption: true,
            toolbar1: 'undo redo | insert | styleselect | bold italic | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample twitter_url facebook_url instagram_url youtube_url',
            fontsize_formats: "8pt 10pt 11pt 12pt 14pt 18pt 24pt 36pt",
            setup: function(editor) {
                editor.on('keyup', function() { fireEmit(); });
                editor.on('change', function() { fireEmit(); });
                editor.on('Undo', function() { fireEmit(); });
                editor.on('Redo', function() { fireEmit(); });

                function fireEmit() {
                    var newContent = editor.getContent();
                    vm.$emit('input', newContent); // Fire an event to let its parent know
                }
            },
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                //'//www.tinymce.com/css/codepen.min.css',
                '{{URL::to("/")}}/css/tinymce.css'
            ],
            forced_root_block_attrs: { "style": "margin: 5px 0;" },
            convert_urls: false,

            // Image Upload
            images_upload_handler: function (blobInfo, success, failure) {
                
                if (vm.imageuploadurl == null) { failure('Image Upload Url is not set.'); }

                var form = new FormData();
                form.append('file', blobInfo.blob(), blobInfo.filename());
    
                var resource = vm.$resource(vm.imageuploadurl);
                resource.save(form).then(function (response) {
                    success(response.data);
                }, function (response) {
                    failure("{{trans('cms.server_error')}}");
                });
            },
            valid_elements : '+*[*]',
            extended_valid_elements: "+iframe[width|height|name|align|class|frameborder|allowfullscreen|allow|src|*]," +
            "script[language|type|async|src|charset]" +
            "img[*]" +
            "embed[width|height|name|flashvars|src|bgcolor|align|play|loop|quality|allowscriptaccess|type|pluginspage]" +
            "blockquote[dir|style|cite|class|id|lang|onclick|ondblclick"
            +"|onkeydown|onkeypress|onkeyup|onmousedown|onmousemove|onmouseout"
            +"|onmouseover|onmouseup|title]"
        });
    },
    watch: {
        value: function (value) {
            var vm = this;
            var editor = tinymce.get(vm.$el.id);
            //Cursor Position Fix
            if(value != editor.getContent()) {
                editor.setContent(value);
            }
        }
    },
    destroyed: function () {
        var vm = this;
        tinymce.remove(vm.$el.id);
    }
});
</script>   