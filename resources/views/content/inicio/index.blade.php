@if($principal)
<div class="container margen">
	<div class="pt-5 mt-5 titulos">
		<h1 class="bookosb ">{!!$principal->titulo!!}</h1>		 

				<p class="text-muted d-inline">{{$principal->autor->nombre}} {{$principal->autor->apellidos}}
				</p>
				<p class="text-muted d-inline float-right">
				{{PageHelpers::dateStringWithDay($principal->created_at)}}
				</p>

	</div>
	<div class="form-group">
		@if ($principal->imagen)
			{!! Html::image($principal->imagen, $principal->titulo, ['class' => 'card-img-top']) !!}
		@endif
		
	</div>

	<div class="form-group">
		<p class="text-justify descripcion">{{$principal->descripcion}}</p>

		<a class="btn border border-primary" href="{{route('publicacion', $principal->id)}}"> <span class="text-primary">LEER MÁS</span></a>
	</div>

	<div class="divisor">
		<hr class="bg-info">
	</div>

	<div class="row d-flex justify-content-between">
		<div class="col-lg-7 col-md-12">

			@foreach ($publicaciones as $publicacion)

				<div class="form-group">
					@if ($publicacion->imagen)
						<small class="d-flex justify-content-end">{{$publicacion->autor->nombre}} {{$publicacion->autor->apellidos}} - {{PageHelpers::dateStringWithDay($publicacion->created_at)}} </small>
						{!! Html::image($publicacion->imagen, $publicacion->titulo, ['class' => 'card-img-top']) !!}
					
					

					<div class="titulos">
						<h3 class="pt-2 mt-2">{!!$publicacion->titulo!!}</h3>
					</div>
					
					<p class="text-justify contenidoIndex descripcion">{{$publicacion->descripcion}}</p>

					<a class="btn border border-warning" href="{{route('publicacion', $publicacion->id)}}" ><span class="text-warning">LEER MÁS</span></a>
					@else

						<div class="titulos">
							<h3 class="pt-2 mt-2">{!!$publicacion->titulo!!}</h3>
							<small class="d-flex justify-content-end">{{$publicacion->autor->nombre}} {{$publicacion->autor->apellidos}} - {{PageHelpers::dateStringWithDay($publicacion->created_at)}} </small>
							<br>
						</div>
						
						<p class="text-justify contenidoIndex descripcion">{{$publicacion->descripcion}}</p>

						<a class="btn border border-warning" href="{{route('publicacion', $publicacion->id)}}" ><span class="text-warning">LEER MÁS</span></a>

					@endif

				</div>

			@endforeach

		</div>
		<div class="col-lg-4 col-md-12" style="padding-bottom: 41px;">
			<div class="color2 p-3">
				<div class="text-center">
					<img class="img_aside w-50" src="images/botella.png" alt="images/botella.png">
					<h2 class="Atlane" style="color: #074a88;">Editor</h2>
				</div>
				<div class="directorio">
					<div class="text-center">
						{{-- <h5 class="arno">Director:</h5> --}}
						<p class="arnoDir">Manuel Lucero</p>
					</div>
				</div>
				{{-- <div class="directorio">
					<div class="text-center">
						<h5 class="arno">Subdirector:</h5>
						<p class="arnoDir">Alejandro Álvarez</p>
					</div>
				</div>
				<div class="directorio">
					<div class="text-center">
						<h5 class="arno">Consejero editorial:</h5>
						<p class="arnoDir">Marco Antonio Landavazo</p>
					</div>
				</div> --}}
			</div>

			<br>
			<div class="color2 p-3">
				<img class="img-fluid" src="images/servicio.jpg" alt="images/botella.png">
			</div>

			{{-- <div>
				<div class="text-center mt-5 pt-3">
					<h2 class="Atlane">Más Leídos del mes</h2>
					<hr class="bg-info">
				</div>
					@foreach($leidasMes as $leidas)
						<div class="pt-3">
							@if ($leidas->imagen)
								{!! Html::image($leidas->imagen, $leidas->titulo, ['class' => 'img-fluid']) !!}
							@endif
							
							<div class="text-muted d-flex justify-content-between">
								<small>{{$leidas->autor->nombre}} {{$leidas->autor->apellidos}}</small>
								<small>
									{{PageHelpers::dateString($leidas->created_at)}}								
								</small>
							</div>
							<div class="tituloMasLeidas">
								<h5 class="pt-2 mt-2 ">{!!$leidas->titulo!!}</h5>
							</div>		
							@if (!$leidas->imagen)
								{{$leidas->descripcion}}
								<br>	
							@endif					
							<a class="btn border border-warning" href="{{route('publicacion', $leidas->id)}}" ><span class="text-warning">LEER MÁS</span></a>
							<hr>
						</div>
					@endforeach
			</div> --}}

			{{-- <div>
				<div class="text-center mt-5 pt-3">
					<h2 class="Atlane">Más Leídos</h2>
					<hr class="bg-info">
				</div>
					@foreach($masLeidos as $leidas)
						<div class="pt-3">
							@if ($leidas->imagen)
								{!! Html::image($leidas->imagen, $leidas->titulo, ['class' => 'img-fluid']) !!}
							@endif
							
							<div class="text-muted d-flex justify-content-between">
								<small>{{$leidas->autor->nombre}} {{$leidas->autor->apellidos}}</small>
								<small>
									{{PageHelpers::dateString($leidas->created_at)}}								
								</small>
							</div>
							<div class="tituloMasLeidas">
								<h5 class="pt-2 mt-2 ">{!!$leidas->titulo!!}</h5>
							</div>
							@if (!$leidas->imagen)
								{{$leidas->descripcion}}
								<br>	
							@endif									
							<a class="btn border border-warning" href="{{route('publicacion', $leidas->id)}}" ><span class="text-warning">LEER MÁS</span></a>
							<hr>
						</div>
					@endforeach
			</div> --}}
		</div>

	</div>
</div>
@endif