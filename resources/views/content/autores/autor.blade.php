<div class="container">
	
	<div class="row pt-5 pb-5" v-if="autor">
		<div class="col-md-4 col-sm-4 col-xs-4 text-center">
			<img :src="checkImage(autor[0].imagen)" class="imagenAutor img-fluid">
		</div>
		<div class="col-md-8 col-sm-8 col-xs-8 text-center">
			<h1 class= "nombreAutor">@{{autor[0].nombre}} @{{autor[0].apellidos}}</h1>
		</div>
	</div>

	<filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch>

	<div class="row pt-5 mt-5 pb-5">

		<div class="col-lg-4 col-md-12 pt-3" v-for="o in obras">

				<div class="card" v-if="obras">
					<div class="text-center">
						<img :src="checkImage(o.imagen)" class="img-fluid">
						<p class="text-muted d-flex justify-content-end">
							@{{dateString(o.created_at)}}
						</p>
					</div>
				</div>
				 <div class="card-body">
					<h5 class="card-title font-weight-bold" v-html="o.titulo"></h5>
					<a class="btn border border-warning" v-on:click="goto_route(o.id)"><span class="text-warning">LEER MÁS</span></a>					
				</div>
		</div>
	</div>
	
	<div class="box-footer clearfix">
		<nav aria-label="Page navigation example">
        	<pagination @setpage="getData" :param="pagination"></pagination>
    	</nav>

		{{-- <nav aria-label="Page navigation example">
			<ul class="pagination">
				<li class="">
					<a class="page-link" href="#" aria-label="Previous">
					<span aria-hidden="true"> « </span>

					</a>
				</li>
				<li class=""><a class="" href="#">1</a></li>
				<li class=""><a class="page-link" href="#">2</a></li>
				<li class=""><a class="page-link" href="#">3</a></li>
				<li class="">
					<a class="page-link" href="http://localhost/botellaAlMar/public/obrasAutor?page=2" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
					</a>
				</li>
			</ul>
		</nav> --}}

    </div>

</div>