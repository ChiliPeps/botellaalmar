<div class="container">
		<div class="pt-5 mt-5">
				<div class="form-group font-weight-bold titulos">
					<h1 class="">{!!$publicacion->titulo!!}</h1>
				</div>

				<div class="row">
					<div class="col-md-2 col-xs-2">
						{!! Html::image($publicacion->autor->imagen, $publicacion->autor->nombre, ['class' => 'img-fluid logoSize']) !!}
					</div>
					<div class="col-md-4 col-xs-4">
						<a href="{{route('autor', $publicacion->id_autor)}}">
							<p class="fecha_post text-capitalize text3 autorNombreCont">{{$publicacion->autor->nombre}} {{$publicacion->autor->apellidos}}</p>
						</a>
					</div>
					<div class="col-md-6 col-xs-6">
						<p class="text-muted fecha_post fechaCont">
						{{PageHelpers::dateStringWithDay($publicacion->created_at)}}
					</p>
					</div>
				</div>

			<!-- ilustracion de referencia -->
				{!! Html::image($publicacion->imagen, $publicacion->imagen, ['class' => 'card-img-top']) !!}
				<p class="text-muted">{{$publicacion->autor_foto}}</p>
				<div class="row d-flex justify-content-between">
					<div class="col-lg-7">
						<div class="pt-3 ">
							<h3 class="font-weight-bold dedicatoria">“{{$publicacion->epigrafe}}”</h3>
							{{-- <p>autor de epígrafe</p> --}}
						</div>
						<div class="text-right pt-5">
							<p class="h4 text-muted dedicatoria">{{$publicacion->dedicatoria}}</p>
							<br>
						</div>
						<div class="capital text-justify contenido arno">							
								{!!$publicacion->contenido!!}					
						</div>

						{{-- <ul class = botones_compartir>
						  <li>
						  	 <a class="btn-floating btn-lg btn-fb" type="button" role="button"><i class="fab fa-facebook-f"></i></a>
						  </li>

						  
						</ul> --}}
						<div class="buttons pt-5 pb-5">

							<a @click="shareFacebook">
								<i class="btn btnShare fac fab fa-facebook-f fa-lg"></i>
							</a>

							<a @click="shareWhatsApp()">
								<i class="btn btnShare wa fab fa-whatsapp fa-lg"></i>
							</a>

							<a href="" @click="shareTwitter">
								<i class="btn btnShare tw fab fa-twitter fa-lg"></i>
							</a>						
							
						</div>

						{{-- comentarios de facebook --}}
						<div class="fb-comments" data-href="{{$url}}" data-width="200%" data-numposts="50">
						</div>
					</div>
					
					<div class="col-lg-4 col-md-12">
						<div class="color2 p-3">
							<div class="text-center">
								<img class="img_aside w-50" src="{{url('/images/botella.png')}}" alt="Imagenes/botella.png">
								<h1 class="Atlane" style="color: #074a88;">Directorio</h1>
							</div>

							<div class="directorio">
								<div class="text-center">
									<h5 class="arno">Director:</h5>
									<p class="arno">Manuel Lucero</p>
								</div>
							</div>
							<div class="directorio">
								<div class="text-center">
									<h5 class="">Subdirector:</h5>
									<p>Alejandro Álvarez</p>
								</div>
							</div>
							<div class="directorio">
								<div class="text-center">
									<h5 class="">Consejero editorial:</h5>
									<p>Marco Antonio Landavazo</p>
								</div>
							</div>
							
						</div>

						<div>
							<div class="text-center mt-5 pt-3">
								<h2 class="Atlane">Otras obras</h2>
								<hr class="bg-info">
							</div>

							@foreach($mismoAutor as $pub)
							
							<div class="pt-3">
								@if ($pub->imagen)
									{!! Html::image($pub->imagen, $pub->titulo, ['class' => 'img-fluid']) !!}
								@endif
								
								<div class="text-muted d-flex justify-content-between">
									<small>{{$pub->autor->nombre}} {{$pub->autor->apellidos}}</small>
									<small>
										{{PageHelpers::dateString($pub->created_at)}}
									</small>
								</div>
								<h5 class="pt-2 mt-2">{!!$pub->titulo!!}</h5>
								@if (!$pub->imagen)
									<div class="text-muted">
										{{$pub->descripcion}}	
									</div>									
									<br>
								@endif
								<a class="btn border border-warning"href="{{route('publicacion', $pub->id)}}"><span class="text-warning">LEER MÁS</span></a>
							</div>	

							@endforeach				
						</div>					
				</div>
					
			</div>
		</div>
	</div>