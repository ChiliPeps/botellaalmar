<div class="container-fluid" style="background-color: #CFE2DF;padding: 10px;">
    <div class="container">
      <div class="text-center pt-5">
        <h1 class="text1 Atlane">contacto</h1>
      </div>

      <div class="row" v-show="correoEnviado">
        <div class="col-lg-12">
          <center>
              <h2>Gracias!</h2>
              <h4>Lo contactaremos lo más rapido posible.</h4>
          </center>
          <div class="alert alert-success" role="alert">
             <p style="text-align: center;"> Correo Enviado Correctamente</p>
          </div>
        </div>
      </div>

      <div class="col-lg-12">
        <div class="alert alert-danger" v-if="errores">
            <h4><i class="icon fa fa-ban"></i> Error</h4>
            <ul style="text-transform:capitalize;"><li v-for="msg in errores">@{{ msg[0] }}</li></ul>
        </div>
      </div>

      

      <div class="row">
        <div class="col-lg-12">
          <div style="margin:80px auto;width: 100%;padding: 15px;">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="form-control paddin" placeholder="Nombre" v-model="contacto.nombre">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control paddin" placeholder="Email" v-model="contacto.correo">
                </div>
                <div class="form-group">
                  <input type="number" class="form-control paddin" placeholder="Telefono" v-model="contacto.telefono">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <textarea name="" id="" class="form-control margenes" placeholder="Escribe tu mensaje" v-model="contacto.comentario">
                    
                  </textarea>
                </div>
              </div>

              <div class="col-lg-12 text-center" v-show="boton">
                <button class="btn font-weight-bold" id="button_contacto" @click="enviar()">Enviar</button>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
</div>