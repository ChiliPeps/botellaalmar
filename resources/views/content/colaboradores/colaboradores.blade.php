	<div class="container p-5">
		<div class="text-center pt-2 pb-5 pt-5 tituloColaboradorAutor">
			<h1 class="text1 Atlane">Colaboradores</h1>
		</div>
		<div class="mt-5 pt-2">
			@foreach($colaboradores as $autor)

				<div class="container">
			        <div class="profile-teaser media flex-column flex-md-row pb-5">

						{{-- <a href="{{route('autor', $autor->id)}}"> --}}
							{!! Html::image($autor->imagen, $autor->nombre, ['class' => 'profile-image mb-3 mb-md-0 mr-md-5 ml-md-0 rounded mx-auto imgAutor']) !!}
						{{-- </a> --}}

						<div class="media-body text-center text-md-left">
							<h3 class="nombreAutores">	<a href="{{route('autor', $autor->id)}}"> {!!$autor->nombre!!} {!!$autor->apellidos!!}</a></h3>
						<div class="bio mb-3">
							</p>{!!$autor->biografia!!}
						</div>
						</div>		        
			        </div>
	    		</div>
			@endforeach
		</div>
</div>