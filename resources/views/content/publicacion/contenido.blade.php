<div class="container">
		<div class="pt-5 mt-5">
				<div class="form-group font-weight-bold titulos">
					<h1 class="">{!!$publicacion->titulo!!}</h1>
				</div>

				<div class="row">
					<div class="col-lg-2 col-md-2 col-xs-2">
						{!! Html::image($publicacion->autor->imagen, $publicacion->autor->nombre, ['class' => 'img-fluid logoSize']) !!}
					</div>
					<div class="col-lg-4 col-md-4 col-xs-4">
						<a href="{{route('autor', $publicacion->id_autor)}}">
							<p class="fecha_post text-capitalize text3 autorNombreCont">{{$publicacion->autor->nombre}} {{$publicacion->autor->apellidos}}</p>
						</a>
					</div>
					<div class="col-lg-6 col-md-6 col-xs-6">
						<p class="text-muted fecha_post fechaCont">
						{{PageHelpers::dateStringWithDay($publicacion->created_at)}}
					</p>
					</div>
				</div>

			<!-- ilustracion de referencia -->
				@if ($publicacion->imagen)
					{!! Html::image($publicacion->imagen, $publicacion->imagen, ['class' => 'card-img-top']) !!}
				@endif
				
				@if ($publicacion->autor_foto)
					<p class="text-muted">{{$publicacion->autor_foto}}</p>
				@else
					<p class="text-muted">  </p>
				@endif
				
				<div class="row d-flex justify-content-between">
					<div class="col-lg-7">
						@if($publicacion->epigrafe)
						<div class="pt-3 ">							
							<h3 class="font-weight-bold dedicatoria">{{$publicacion->epigrafe}}</h3>					
							{{-- <p>autor de epígrafe</p> --}}
						</div>
						@endif

						@if($publicacion->dedicatoria)
							<div class="text-right pt-5">
								<p class="h4 text-muted dedicatoria">{{$publicacion->dedicatoria}}</p>
								<br>
							</div>
						@endif
						
						<div class="capital text-justify contenido">							
								{!!$publicacion->contenido!!}					
						</div>

						{{-- <ul class = botones_compartir>
						  <li>
						  	 <a class="btn-floating btn-lg btn-fb" type="button" role="button"><i class="fab fa-facebook-f"></i></a>
						  </li>

						  
						</ul> --}}
						<div class="buttons pt-5 pb-5">

							<a @click="shareFacebook">
								<i class="btn btnShare fac fab fa-facebook-f fa-lg"></i>
							</a>

							<a @click="shareWhatsApp()">
								<i class="btn btnShare wa fab fa-whatsapp fa-lg"></i>
							</a>

							<a @click="shareTwitter">
								<i class="btn btnShare tw fab fa-twitter fa-lg"></i>
							</a>						
							
						</div>

						{{-- comentarios de facebook --}}
						<div class="fb-comments" data-href="{{$url}}" data-width="100%" data-numposts="20"></div>
	
					</div>
					
					<div class="col-lg-4 col-md-12">
						<div class="text-center contenido" style="color:#074a89;">
							<p style="color:#074a89 !important; font-size:35px !important;">Eres el Lector</p>
							<h1>{!!$publicacion->hits!!}</h1>
						</div>
						<div class="color2 p-3">
							<div class="text-center">
								<img class="img_aside w-50" src="../images/botella.png" alt="images/botella.png">
								<h2 class="Atlane" style="color: #074a88;">Editor</h2>
							</div>
							<div class="directorio">
								<div class="text-center">
									{{-- <h5 class="arno">Director:</h5> --}}
									<p class="arnoDir">Manuel Lucero</p>
								</div>
							</div>
							{{-- <div class="directorio">
								<div class="text-center">
									<h5 class="arno">Subdirector:</h5>
									<p class="arnoDir">Alejandro Álvarez</p>
								</div>
							</div>
							<div class="directorio">
								<div class="text-center">
									<h5 class="arno">Consejero editorial:</h5>
									<p class="arnoDir">Marco Antonio Landavazo</p>
								</div>
							</div> --}}
						</div>

						<br>
						<div class="color2 p-3">
							<img class="img-fluid" src="../images/servicio.jpg" alt="images/botella.png">
						</div>

						<div>
							<div class="text-center mt-5 pt-3">
								<h2 class="Atlane">Otros textos</h2>
								<hr class="bg-info">
							</div>

							@foreach($mismoAutor as $pub)
							
							<div class="pt-3">
								@if ($pub->imagen)
									{!! Html::image($pub->imagen, $pub->titulo, ['class' => 'img-fluid']) !!}
								@endif
								
								<div class="text-muted d-flex justify-content-between">
									<small>{{$pub->autor->nombre}} {{$pub->autor->apellidos}}</small>
									<small>
										{{PageHelpers::dateString($pub->created_at)}}
									</small>
								</div>
								<h5 class="pt-2 mt-2">{!!$pub->titulo!!}</h5>
								@if (!$pub->imagen)
									<div class="text-muted">
										{{$pub->descripcion}}	
									</div>	
								<br>	
								@endif		
								<a class="btn border border-warning"href="{{route('publicacion', $pub->id)}}"><span class="text-warning">LEER MÁS</span></a>
								<hr>
							</div>	

							@endforeach				
						</div>					
				</div>
					
			</div>
		</div>
	</div>