<meta name="author" content="{{ $publicacion->autor->nombre }} {{ $publicacion->autor->apellidos }}">

{{-- "Open Graph" - Para Facebook --}}
<meta property="fb:app_id" content="{{env('FACEBOOK_APP_ID')}}"/>
<meta property="og:locale" content="es_MX">
<meta property="og:type" content="website">
<meta property="og:title" content="{{ $linetxt }} - Botella al Mar" >
<meta property="og:description" content="{{ $publicacion->descripcion }}">
<meta property="og:url" content="{{ $url }}"/>
<meta property="og:site_name" content="Botella al mar"/>

{{-- Image --}}
<meta property="og:image" content="{{$imagen}}"/>
<meta property="og:image:width" content="800"/>
<meta property="og:image:height" content="500"/>

{{-- Twitter Metas --}}
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="{{$publicacion->descripcion}}"/>
<meta name="twitter:title" content="{{$linetxt}} - Botella al Mar"/>
<meta name="twitter:site" content="Botella al Mar"/>
<meta name="twitter:image" content="{{$imagen}}"/>
<meta name="twitter:creator" content="{{ $publicacion->autor->nombre }}"/>