{!! CMSHelpers::makeLinkForSidebarMenu('admin.configuraciones.index', 'Configuración', 'fa fa-cogs') !!}
{!! CMSHelpers::makeLinkForSidebarMenu('admin.errores.index', 'Bítacora de Errores', 'fa fa-bomb') !!}
{!! CMSHelpers::makeLinkForSidebarMenu('admin.autores.index', 'Autores', 'fa fa-user') !!}
{!! CMSHelpers::makeLinkForSidebarMenu('admin.publicaciones.index', 'Publicaciones', 'fa fa-edit') !!}
