@extends('cms.master')

@section('content')
    <section class="content-header">
        <h1><i class="fa fa-edit"></i> Publicaciones </h1>
    </section>

    <section class="content" id="app" v-cloak>
    	<div class="box box-primary" v-show="panelIndex">

            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <a href="#" class="btn bg-navy" @click="openPanelInputs">
                    <i class="fa fa-plus-circle"></i> Nueva Publicacion</a>
                </div>

                <filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch>
            </div>

            <div class="box-body">

                <!-- VueLoading icon -->
                <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div>

                <div class="table-responsive" v-show="!loading">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="2">Título</th>
                            <th>Autor</th>
                            <th>Hits</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="noticia in publicaciones">
                            <td v-if="noticia.thumb != null" style="width: 85px;"><img :src="checkImage(noticia.thumb)" class="img-rounded" width="75" height="50"></td>
                            <td v-else style="width: 85px;">
                                <img src="https://dummyimage.com/600x400/000/ffffff&text=NO+IMAGE" class="img-rounded" width="75" height="50">
                            </td>

                            <td>
                                <div>
                                    <div style="color: #3c8dbc; font-weight: bold;" :title="noticia.titulo" v-html="noticia.titulo"></div>
                                    <div>@{{ dateString2(noticia.fecha) }}</div>
                                    <div></div>
                                </div>
                            </td>
                            <td>@{{ noticia.autor.nombre}}  @{{noticia.autor.apellidos}}</td>

                            <td>@{{ noticia.hits }}</td>
                            <td>
                                <button v-if="noticia.publish == 1" class="btn btn-xs btn-success" @click="togglePublish(noticia.id)" 
                                style="margin-bottom: 10px;" title="No Publicar"><i class="fa fa-eye"></i></button>

                                <button v-else class="btn btn-xs btn-danger" @click="togglePublish(noticia.id)" 
                                style="margin-bottom: 10px;" title="Publicar"><i class="fa fa-eye-slash"></i></button>

                                {{-- <button class="btn btn-xs btn-default" @click="openProgramarNoticia(noticia.id)" 
                                style="margin-bottom: 10px;" title="Programar Noticia"><i class="fa fa-clock-o"></i></button> --}}

                                <button class="btn btn-xs btn-info" @click="previewNoticia(noticia.id)" 
                                style="margin-bottom: 10px;" title="Previsualizar Noticia"><i class="fa fa-binoculars"></i></button>

                                <button class="btn btn-xs btn-warning" @click="openUpdateInputPanel(noticia.id)"
                                style="margin-bottom: 10px;" title="Actualizar Noticia"><i class="fa fa-refresh"></i></button>

                                <button class="btn btn-xs btn-danger" @click="deletePublicacion(noticia.id)" 
                                style="margin-bottom: 10px;" title="Borrar Noticia"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>

            <div class="box-footer clearfix">
                <pagination @setpage="getData" :param="pagination"></pagination>
            </div>
            
        </div>

        {{-- Panel de Inputs --}}
        <div v-show="panelInputs">
            @include('cms.publicaciones.partials.inputs')
        </div>

    </section>
@endsection

@push('scripts')
    @include('cms.publicaciones.partials.scripts')
@endpush