{{-- Mixins --}}
@include('components.vue.vueNotifications')
@include('components.vue.vueHelperFunctions')

{{-- Components --}}
@include('components.vue.vueFormErrors')
@include('components.vue.vueImageUpload')
@include('components.vue.vuePagination')
@include('components.vue.vueFilterSearch')
@include('components.vue.vueProgress')
@include('components.vue.vueTinyMce')

{{-- Vue-Tabs & Css: https://github.com/cristijora/vue-tabs --}}
@include('components.vue.vueTabs')

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

var app = new Vue ({
    mounted: function () {
        this.getData(this.dataRoute);
        
        // Fecha actual
        var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
        var localISOTime = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1).slice(0, 10);
        this.fechaAhora = localISOTime;
    },
    el: '#app',
    components: { 
        'vue-tabs': VueTabs, // Vue-tabs Component
        'v-tab': VTab // Vue-tabs Component
    }, 
    mixins: [helperFunctions, notifications],
    data: {
        fechaAhora: '',
        autores: null,
        autor: { id: '', nombre: '', apellidos: '', tipo:'', facebook: '', imagen: '',
        twitter: '', instagram: '', url: '', correo: '', biografia: '' },
        pagination: null,
        loading: false,
        loadingSave: false,
        saveInAction: false,
        public_url: "{{ URL::to('/') }}/",
        saveButton: false,
        updateButton: false,
        uploadProgress: null,
        tabName: '', // vue-tabs var

        // Panels
        panelIndex: true,
        panelInputs: false,

        // Data Route
        dataRoute: "{{route('admin.autores.get')}}",

        // Filter
        busqueda: '',
        tipo: 'nombre',
        tipos:[
            { text: 'Nombre', value:'nombre' },
            { text: 'Apellidos', value:'apellidos' }
        ],

        // Formulario errores
        errores: null
    },
    computed: {
        desCharCount: function () {
            return this.noticia.descripcion.length;
        }
    },
    methods: {
        getData: function (url) {
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo };
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                this.autores = response.data.data;
                this.loading = false;
            });
        },

        updateFilters: function (data) {
            this.busqueda = data.busqueda;
            this.tipo = data.tipo;
            this.getData(this.dataRoute);
        },

        // Inputs
        openPanelInputs: function() {
            this.saveButton = true;
            this.panelIndex = false;
            this.panelInputs = true;
        },

        closePanelInputs: function () {
            this.cleanInputs();
            this.cleanErrors();
            this.panelInputs = false;
            this.panelIndex = true;
            this.saveButton = false;
            this.updateButton = false;
            this.tabName = 'Datos del Autor';
        },

        setData: function () {
            if (this.saveInAction == true) { return; }
            this.saveInAction = true;
            this.loadingSave = true;
            this.errores = null;

            var form = new FormData();
            form.append('nombre',    this.autor.nombre);
            form.append('apellidos', this.autor.apellidos);
            form.append('tipo',      this.autor.tipo);
            form.append('facebook',  this.autor.facebook);
            form.append('twitter',   this.autor.twitter);
            form.append('instagram', this.autor.instagram);
            form.append('url',       this.autor.url);
            form.append('correo',    this.autor.correo);
            form.append('biografia', this.autor.biografia);
            if(this.autor.imagen != '') { form.append('imagen', this.autor.imagen); }

            //Upload Progress Bar
            const progress = (e) => {
                if (e.lengthComputable) {
                    this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
                }
            }

            var resource = this.$resource("{{route('admin.autores.set')}}", {}, {}, { progress: progress });
            resource.save(form).then(function (response) {
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "Autor agregado correctamente.", 'topCenter');
                this.cleanErrors();
                this.closePanelInputs();
                this.getData(this.dataRoute);
                this.uploadProgress = null;
            }, function (response) {
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
                this.uploadProgress = null;
            });
        },

        openUpdateInputPanel: function (idAutor) {
            var index = _.findIndex(this.autores, function(o) { return o.id === idAutor; });

            this.autor = {
                id:         this.autores[index].id,
                nombre:     this.autores[index].nombre,
                apellidos:  this.autores[index].apellidos,
                tipo:       this.autores[index].tipo,
                facebook:   this.autores[index].facebook,
                twitter:    this.autores[index].twitter,
                instagram:  this.autores[index].instagram,
                url:        this.autores[index].url,
                correo:     this.autores[index].correo,
                biografia:  this.autores[index].biografia,
                imagen:     this.autores[index].imagen
            }

            this.updateButton = true;
            this.panelIndex = false;
            this.panelInputs = true;
        },

        updateData: function () {
            if(this.saveInAction == true) { return; }
            this.saveInAction = true;
            this.loadingSave = true;
            this.errores = null;

            var form = new FormData();
            form.append('id',        this.autor.id);
            form.append('nombre',    this.autor.nombre);
            form.append('apellidos', this.autor.apellidos);
            form.append('tipo',      this.autor.tipo);
            form.append('facebook',  this.autor.facebook);
            form.append('twitter',   this.autor.twitter);
            form.append('instagram', this.autor.instagram);
            form.append('url',       this.autor.url);
            form.append('correo',    this.autor.correo);
            form.append('biografia', this.autor.biografia);
            
            //Is File ?? o String ??
            if(this.autor.imagen != '') {
                if(Object.prototype.toString.call(this.autor.imagen) === '[object File]' ) {
                    form.append('imagen', this.autor.imagen);
                } else { form.append('imagen', 'no valid'); }
            }

            //Upload Progress Bar
            const progress = (e) => {
                if (e.lengthComputable) {
                    this.uploadProgress = Math.floor(e.loaded  / e.total * 100);
                }
            }

            var resource = this.$resource("{{route('admin.autores.update')}}", {}, {}, { progress: progress });
            resource.save(form).then(function (response) { 
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-check-circle', 'OK!', "Autor actualizado correctamente.", 'topCenter');
                this.cleanErrors();
                this.closePanelInputs();
                this.getData(this.dataRoute);
                this.uploadProgress = null;
            }, function (response) {
                this.loadingSave = false;
                this.saveInAction = false;
                this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                this.errores = response.data.errors;
                this.uploadProgress = null;
            });
        },

        deleteAutor: function (idAutor) {
            swal({
                title: '{{trans("cms.are_you_sure")}}',
                text: 'No podras revertir esto. También se borrarán todas sus publicaciones',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, borralo!',
                cancelButtonText: '{{trans("cms.cancel")}}'
                }).then(function () {
                    var resource = this.$resource("{{route('admin.autores.delete')}}");
                    resource.delete({id:idAutor}).then(function (response) {
                        this.notification('fa fa-check-circle', 'OK!', "Autor Borrado!", 'topCenter');
                        this.getData(this.dataRoute);
                    }, function (response) {
                        this.notification('fa fa-exclamation-triangle', 'Error', "{{trans('cms.server_error')}}", 'topCenter');
                        this.checkExtraErrorMessages(response);
                    });
            }.bind(this)).catch(swal.noop);
        },

        cleanErrors: function () {
            this.errores = null;
        },

        cleanInputs: function () {
            this.autor = { id: '', nombre: '', apellidos: '', tipo:'', facebook: '', imagen: '',
            twitter: '', instagram: '', url: '', correo: '', biografia: '' };
        },

        checkImage: function (image_url) {
            if (!/^(f|ht)tps?:\/\//i.test(image_url)) {
                return this.public_url+image_url
            } else { return image_url }
        },
        
        checkExtraErrorMessages: function (response) {
            if (response.body && response.body.errorCode) {
                this.notification('fa fa-exclamation-triangle', 'Error', 'No tienes permisos suficientes como reportero.', 'topCenter');
            }
        }
    }
});
</script>