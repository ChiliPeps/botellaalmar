<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title" v-if="saveButton"> Crear Autor</h3>
        <h3 class="box-title" v-if="updateButton"> Actualizar Autor</h3>
        <div class="box-tools pull-right">
            <a class="btn bg-navy btn-sm" @click="closePanelInputs"><i class="fa fa-chevron-left"></i> Regresar</a>
        <a class="btn btn-success btn-sm" v-show="saveButton" @click="setData"><i class="fa fa-floppy-o"></i> Crear Autor</a>
            <a class="btn btn-success btn-sm" v-show="updateButton" @click="updateData"><i class="fa fa-floppy-o"></i> Actualizar Autor</a>
        </div>
    </div>
</div>

{{-- Upload Progress Bar --}}
<vprogress :progress="uploadProgress"></vprogress>

{{-- Form Errros --}}
<formerrors :errorsbag="errores"></formerrors>

<vue-tabs v-model="tabName">
    <br>
    <v-tab title="Datos del Autor">
        <div class="row">
            <div class="col-md-6">
        
                <div class="box box-primary">
                    <div class="box-body">
        
                        {{-- nombre --}}
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" 
                            placeholder="nombre del autor" v-model="autor.nombre" 
                            :class="formError(errores, 'nombre', 'inputError')">
                        </div>

                        {{-- apellidos --}}
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" class="form-control" 
                            placeholder="apellidos del autor" v-model="autor.apellidos" 
                            :class="formError(errores, 'apellidos', 'inputError')">
                        </div>

                        {{-- correo --}}
                        <div class="form-group">
                            <label>Correo</label>
                            <input type="text" class="form-control" 
                            placeholder="correo del autor" v-model="autor.correo" 
                            :class="formError(errores, 'correo', 'inputError')">
                        </div>

                        <div class="form-group">
                            <label>Tipo</label>
                            <select class="form-control" v-model="autor.tipo" 
                                :class="formError(errores, 'tipo', 'inputError')">
                                <option  value="editor">Editor</option>
                                <option  value="colaborador">Colaborador</option>
                            </select>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-md-6">
        
                <div class="box box-danger">
                    <div class="box-body">
        
                        {{-- facebook --}}
                        <div class="form-group">
                            <label>Url Facebook (opcional)</label>
                            <input type="text" class="form-control" 
                            placeholder="url de facebook del autor" v-model="autor.facebook" 
                            :class="formError(errores, 'facebook', 'inputError')">
                        </div>

                        {{-- twitter --}}
                        <div class="form-group">
                            <label>Url de Twitter (opcional)</label>
                            <input type="text" class="form-control" 
                            placeholder="url del twitter del autor" v-model="autor.twitter" 
                            :class="formError(errores, 'twitter', 'inputError')">
                        </div>

                        {{-- instagram --}}
                        <div class="form-group">
                            <label>Url Instagram (opcional)</label>
                            <input type="text" class="form-control" 
                            placeholder="url del instagram del autor" v-model="autor.instagram" 
                            :class="formError(errores, 'instagram', 'inputError')">
                        </div>

                        {{-- url --}}
                        <div class="form-group">
                            <label>Url Página Web (opcional)</label>
                            <input type="text" class="form-control" 
                            placeholder="url de la página web del autor" v-model="autor.url" 
                            :class="formError(errores, 'url', 'inputError')">
                        </div>

                    </div>
                </div>
        
            </div>
        </div>
    </v-tab>

    <v-tab title="Biografía del Autor">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <label>Biografía</label>
                    <tinymce id="tinyMCE" imageuploadurl="{{route('admin.noticias.tinyimages')}}" 
                    v-model="autor.biografia"></tinymce>
                </div>
            </div>
        </div>
    </v-tab>

    <v-tab title="Imagen del Autor">
        <div class="box box-primary">
            <div class="box-header"><h2 class="box-title">Imagen del Autor (300 x 300)</h2></div>
            <div class="box-body">
                {{-- Image Upload --}}
                <imageupload v-model="autor.imagen"></imageupload>
            </div>
        </div>
    </v-tab>
</vue-tabs>