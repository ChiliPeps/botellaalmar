@extends('cms.master')

@section('content')
    <section class="content-header">
        <h1><i class="fa fa-user"></i> Autores </h1>
    </section>

    <section class="content" id="app" v-cloak>

        <div class="box box-primary" v-show="panelIndex">

            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <a href="#" class="btn bg-navy" @click="openPanelInputs">
                    <i class="fa fa-plus-circle"></i> Nuevo Autor</a>
                </div>

                <filtersearch :selected="tipo" :options="tipos" @updatefilters="updateFilters"></filtersearch>
            </div>

            <div class="box-body">

                <!-- VueLoading icon -->
                <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div>

                <div class="table-responsive" v-show="!loading">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Imagen</th>
                            <th>Nombre & Apellidos</th>
                            <th>Links</th>
                            <th>Correo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="autor in autores">
                            <td style="width: 85px;"><img :src="checkImage(autor.imagen)" class="img-rounded" width="50" height="50"></td>
                            <td>@{{ autor.nombre }} @{{ autor.apellidos }}</td>
                            <td>
                                <a class="btn btn-xs btn-info" v-if="autor.facebook" :href="autor.facebook" 
                                target="_blank" :title="autor.facebook"><i class="fa fa-facebook-square"></i></a>
                                <a class="btn btn-xs btn-info" v-if="autor.twitter" :href="autor.twitter" 
                                target="_blank" :title="autor.twitter"><i class="fa fa-twitter-square"></i></a>
                                <a class="btn btn-xs btn-info" v-if="autor.instagram" :href="autor.instagram"
                                target="_blank" :title="autor.instagram"><i class="fa fa-instagram"></i></a>
                                <a class="btn btn-xs btn-info" v-if="autor.url" :href="autor.url" 
                                target="_blank" :title="autor.url"><i class="fa fa-internet-explorer"></i></a>
                            </td>
                            <td>
                                <div v-if="autor.correo"><a :href="'mailto:'+autor.correo">@{{ autor.correo }}</a></div>
                                <div v-else>Sín Correo</div>
                            </td>
                            <td>
                                <button class="btn btn-xs btn-warning" @click="openUpdateInputPanel(autor.id)"
                                style="margin-bottom: 10px;" title="Editar Autor"><i class="fa fa-refresh"></i></button>

                                <button class="btn btn-xs btn-danger" @click="deleteAutor(autor.id)" 
                                style="margin-bottom: 10px;" title="Borrar Autor"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>

            <div class="box-footer clearfix">
                <pagination @setpage="getData" :param="pagination"></pagination>
            </div>
            
        </div>

        {{-- Panel de Inputs --}}
        <div v-show="panelInputs">
            @include('cms.autores.partials.inputs')
        </div>

    </section>
@endsection

@push('scripts')
    @include('cms.autores.partials.scripts')
@endpush