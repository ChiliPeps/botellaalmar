<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {!! Html::favicon('images/favicon/favicon.ico') !!}
        <title>{{config('app.name')}} - 503</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        {{-- Get Imagen Fondo --}}
        @php
            $conf = CMSHelpers::getImagenMantenimiento();
            $url = url('/').'/'.$conf->imagen_mantenimiento;
        @endphp

        <!-- Styles -->
        <style>
            body { margin: 0; }
            .full-height { height: 100vh; }
            .backfoto {
                background: url({{$url}});
                background-attachment: fixed;
                background-position: center;
                background-size: cover;
            }
        </style>
    </head>
    <body>
        <div class="full-height backfoto"></div>
    </body>
</html>
