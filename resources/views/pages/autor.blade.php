@extends('master')

@section('content')
    <div id="contentapp" v-cloak>
        @include('inc.banner')
        @include('content.autores.autor')
    </div>
@stop

{{-- Vue Script --}}
@push('scripts')
{{-- Components --}}
@include('components.vue.vueHelperFunctions')
@include('components.vue.vuePaginationDos')
@include('components.vue.vueSearch')

<script>
	var contentapp = new Vue({
    mounted: function () { this.getData(this.dataRoute), this.getAutor();},

    el: '#contentapp',
    mixins: [helperFunctions],
    data: {
        
        idAutor:window.location.pathname.split("/").pop(),
        loading:false,
        public_url:"{{ URL::to('/') }}/",
        dataRoute: "{{route('obrasAutor')}}",
        obras:null,
        pagination:null,
        autor:null,
        // currentUrl: window.location.pathname.split("/").pop(),

         // Filter
        busqueda: '',
        tipo: 'titulo',
        tipos:[
            { text: 'Titulo', value:'titulo' },
            { text: 'Apellidos', value:'apellidos' }
        ],

    },
    computed: {
        
    },
    methods: { 
        getData: function (url) {
            this.loading = true;
            var filter = { busqueda: this.busqueda, tipo: this.tipo, id_autor: this.idAutor};
            var resource = this.$resource(url);
            resource.get(filter).then(function (response) {
                this.pagination = response.data;
                this.obras = response.data.data;
                this.loading = false;
            });
        },

        getAutor: function (url) {
            this.loading = true;
            var filter = {id_autor: this.idAutor};
            var resource = this.$resource("{{route('getAutor')}}");            
            resource.get(filter).then(function (response) {
                this.autor = response.data;
                this.loading = false;
            });
        },

        updateFilters: function (data) {
            this.busqueda = data.busqueda;
            this.tipo = data.tipo;
            this.getData(this.dataRoute);
        },

        checkImage: function (image_url) {
            if (image_url) {
                if (!/^(f|ht)tps?:\/\//i.test(image_url)) {
                    return this.public_url+image_url
                } else { return image_url }
            }
            else {
                return this.public_url+'images/noimage.jpg'
            }
        },

        dateString: function (fecha) {
            if(fecha == null){return;}
            var meses = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            var diasSemana = new Array("Domingo", "Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
            var f = new Date(fecha.replace(/-/g,"/")); //Fix UTC "-" to Local "/"
            return diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear();
        },

        goto_route: function (param1) {
	      route = '{{ route('publicacion', ['id' => '?anytagtoreplace?']) }}'
	      location.href = route.replace('?anytagtoreplace?', param1)
	  	},
    }
});

</script>
@endpush