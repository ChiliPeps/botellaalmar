@extends('master')

@section('content')
	<div id="contentapp" v-cloak>
        @include('inc.banner')

    	@include('content.inicio.index')
    </div>
@stop

{{-- Vue Script --}}
@push('scripts')
{{-- Components --}}
<script>
	var contentapp = new Vue({
    mounted: function () {},

    el: '#contentapp',
    data: {
        
        public_url:"{{ URL::to('/') }}/",
    },
    computed: {
        
    },
    methods: {   
        dateString: function (fecha) {
            if(fecha == null){return;}
            var meses = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            var diasSemana = new Array("Domingo", "Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
            var f = new Date(fecha.replace(/-/g,"/")); //Fix UTC "-" to Local "/"
            return diasSemana[f.getDay()] + ", " + f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear();
        },
    }
});

</script>
@endpush




