@extends('master')

@php
    $url = url("/publicaciones/".$publicacion->id);
    $imagen = url('/')."/".$publicacion->imagen;
    $title = utf8_decode($linetxt);

@endphp

@section('title')
    {{$title}} - 
@stop

@section('metas')
    @include('content.publicacion.metas')
@stop

@section('content')
	<div id="contentapp" v-cloak>        
    	@include('content.publicacion.contenido')
    </div>
@stop

{{-- Vue Script --}}
@push('scripts')
{{-- Components --}}
<script>

	var contentapp = new Vue({
    mounted: function () {},

    el: '#contentapp',
    data: {
        
        public_url: "{{ URL::to('/') }}/",
        fecha_now: '',
        noticia_url: "{{$url}}",
        imagen:"{{$imagen}}",
        noticia_titulo: "{{$title}}",

    },
    computed: {

    },
    methods: {   

        shareFacebook: function () {
            window.open("https://facebook.com/sharer.php?display=popup&u=" + this.noticia_url, "facebook_share", 
            "height=320, width=640, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
        },

        shareTwitter: function () {
            window.open("https://twitter.com/share?text="+this.noticia_titulo+"&url="+this.noticia_url, "twitter_share",
            "height=320, width=640, toolbar=no, menubar=no, scrollbars=yes, resizable=yes, location=no, directories=no, status=no")
        },

        shareWhatsApp: function () {
            window.open("https://wa.me/?text="+this.noticia_url);
        },

        shareEmail: function () {
            location.href = "mailto:?subject="+this.noticia_titulo+"&body="+this.noticia_url;
        },

        urlGenerator: function (publicacion) {
           return this.public_url + 'publicacion/' + publicacion.id;
        },

    }
});

</script>


@endpush