@extends('master')

@section('content')
    <div id="contentapp" v-cloak>

		@include('inc.banner')
        @include('content.autores.autores')
    </div>
@stop