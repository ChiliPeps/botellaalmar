@extends('master')

@php
    $url = url("/preview/".$publicacion->id);
@endphp

@section('title')
{{$publicacion->titulo}} - 
@stop

@section('content')
    <div class="container" id="contentapp" v-cloak>
        @include('content.preview.contenido')
    </div>
@stop