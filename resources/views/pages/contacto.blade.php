@extends('master')

@section('content')
	<div id="contentapp" v-cloak>
        @include('inc.banner')

    	@include('content.contacto.contacto')
    </div>
@stop

{{-- Vue Script --}}
@push('scripts')
{{-- Components --}}
<script>
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#csrf-token').getAttribute('content');
	var contentapp = new Vue({
    mounted: function () {},

    el: '#contentapp',
    data: {
        
        contacto:{nombre:'', telefono:'', correo:'', comentario:''},
        correoEnviado:false,
        boton:true,
        loading:false,
        saveInAction: false,
        errores:null,
        public_url:"{{ URL::to('/') }}/",
    },
    computed: {
        
    },
    methods: {   
         urlGenerator: function (publicacion) {
           return this.public_url + 'publicacion/' + publicacion.id;
        },

         enviar: function () {
            if(this.saveInAction == true) { return; }
            this.saveInAction = true;
            this.loading = true;
            this.errores = null;

            var form = new FormData();
            form.append('nombre', this.contacto.nombre);
            form.append('correo', this.contacto.correo);
            form.append('telefono', this.contacto.telefono);
            form.append('comentario', this.contacto.comentario);

            var resource = this.$resource("{{route('contacto.send')}}");
            resource.save(form).then(function (response) {
                this.loading = false;
                this.limpiar();
                this.saveInAction = false;
                this.correoEnviado = true;
                this.boton = false;
            }, function (response) {
                this.loading = false;
                this.saveInAction = false;

                // Checar si hay configuración
                if (response.data.nodata) {
                    alert(response.data.error);
                } else {
                    this.errores = response.data.errors;
                }
            });
        },

        onlyNumber ($event) {
           //console.log($event.keyCode); //keyCodes value
           let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
           if ((keyCode < 48 || keyCode > 57) && keyCode !== 46) { // 46 is dot
              $event.preventDefault();
           }
        },

        limpiar: function(){
            this.contacto = {nombre:'', telefono:'', correo:'', comentario:''}
        }
    }
});

</script>
@endpush